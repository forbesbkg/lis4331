package com.example.breespc.mortgageinterestcalculator;

import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Typeface;
import android.preference.PreferenceManager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.app.ActionBar;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

public class MainActivity extends AppCompatActivity {

    int years, principal, monthly;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        ActionBar actionBar = getSupportActionBar();
        actionBar.setBackgroundDrawable(getResources().getDrawable(R.drawable.gradient));


        Typeface muliSemiBold = Typeface.createFromAsset(getAssets(), "fonts/muli_semibold.ttf");
        Typeface muliBold = Typeface.createFromAsset(getAssets(), "fonts/muli_bold.ttf");

        Button btnCalculate = (Button) findViewById(R.id.btnCalculate);
        final EditText inputPayment = (EditText) findViewById(R.id.inputPayment);
        final EditText inputYears = (EditText) findViewById(R.id.inputYears);
        final EditText inputPrincipal = (EditText) findViewById(R.id.inputPrincipal);

        final SharedPreferences sharedPref = PreferenceManager.getDefaultSharedPreferences(this);


        inputPayment.setTypeface(muliSemiBold);
        inputYears.setTypeface(muliSemiBold);
        inputPrincipal.setTypeface(muliSemiBold);
        btnCalculate.setTypeface(muliBold);



        btnCalculate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v){
                monthly = Integer.parseInt(inputPayment.getText().toString());
                years = Integer.parseInt(inputYears.getText().toString());
                principal = Integer.parseInt(inputPrincipal.getText().toString());

                SharedPreferences.Editor editor = sharedPref.edit();
                editor.putInt("key1", monthly);
                editor.putInt("key2", years);
                editor.putInt("key3", principal);
                editor.commit();

                if (years == 10 || years == 20 || years == 30){
                    startActivity(new Intent(MainActivity.this, SuccessActivity.class));
                }
                else{
                    startActivity(new Intent(MainActivity.this, ErrorActivity.class));
                }
            }
        });
    }
}
