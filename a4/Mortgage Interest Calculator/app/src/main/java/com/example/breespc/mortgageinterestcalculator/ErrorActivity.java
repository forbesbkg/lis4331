package com.example.breespc.mortgageinterestcalculator;

import android.graphics.Typeface;
import android.os.Bundle;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.TextView;

public class ErrorActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_error);

        ActionBar actionBar = getSupportActionBar();
        actionBar.setBackgroundDrawable(getResources().getDrawable(R.drawable.gradient));

        Typeface muliSemiBold = Typeface.createFromAsset(getAssets(), "fonts/muli_semibold.ttf");
        Typeface muliBold = Typeface.createFromAsset(getAssets(), "fonts/muli_bold.ttf");

        TextView txtWhoops = (TextView) findViewById(R.id.txtWhoops);
        TextView txtError1 = (TextView) findViewById(R.id.txtError1);
        TextView txtError2 = (TextView) findViewById(R.id.txtError2);

        txtWhoops.setTypeface(muliBold);
        txtError1.setTypeface(muliSemiBold);
        txtError2.setTypeface(muliSemiBold);


    }

}
