package com.example.breespc.mortgageinterestcalculator;

import android.content.SharedPreferences;
import android.graphics.Typeface;
import android.preference.Preference;
import android.preference.PreferenceManager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.ImageView;
import android.widget.TextView;

import java.text.DecimalFormat;

public class SuccessActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_success);

        Typeface muliSemiBold = Typeface.createFromAsset(getAssets(), "fonts/muli_semibold.ttf");
        Typeface muliBold = Typeface.createFromAsset(getAssets(), "fonts/muli_bold.ttf");

        TextView resultPayment = (TextView) findViewById(R.id.resultPayment);
        TextView resultYears = (TextView) findViewById(R.id.resultYears);
        TextView resultPrincipal = (TextView) findViewById(R.id.resultPrincipal);
        TextView resultTotal = (TextView) findViewById(R.id.resultTotal);

        ImageView imgYears = (ImageView) findViewById(R.id.imgNumber);

        SharedPreferences sharedPref = PreferenceManager.getDefaultSharedPreferences(this);

        int monthly = sharedPref.getInt("key1", 0);
        int years = sharedPref.getInt("key2", 0);
        int principal = sharedPref.getInt("key3", 0);
        double totalPayment;

        totalPayment = (years * (monthly * 12)) - principal;
        DecimalFormat currency = new DecimalFormat("$###,###.##");

        resultPayment.setTypeface(muliSemiBold);
        resultYears.setTypeface(muliSemiBold);
        resultPrincipal.setTypeface(muliSemiBold);
        resultTotal.setTypeface(muliBold);

        if (years == 10){
            imgYears.setImageResource(R.drawable.ten);
        }
        else if (years == 20){
            imgYears.setImageResource(R.drawable.twenty);
        }
        else if (years == 30){
            imgYears.setImageResource(R.drawable.thirty);
        }

        resultPayment.setText("Monthly Payment: " + monthly);
        resultYears.setText("Amount of Years: " + years);
        resultPrincipal.setText("Principal Amount: " + principal);

        resultTotal.setText("Total paid: " + currency.format(totalPayment));


    }
}
