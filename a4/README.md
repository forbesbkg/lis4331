# LIS4331 - Advanced Mobile Web Applications Development

## Bretania Forbes

### Assignment 4 Requirements:

*Mortgage calculator app that calculates amount paid over certain number of years*

1. Include splash screen image, app title, intro text.
2. Include appropriate images.
3. Must use persistent data: SharedPreferences.
4. Widgets and images must be vertically and horizontally aligned.
5. Must add background color(s) or theme.
6. Create and display launched icon image


### README.md includes the following items:
* Screenshots of running Mortgage Calculator App
* Repo and assignment links


### Assignment Screenshots:

*Screenshots of running Mortgage Calculator App* 

|![Splash screen](img/1.png) | ![Homepage](img/2.png)


|![Error](img/3.png) | ![Success](img/4.png)

|![20](img/5.png) | ![30](img/6.png)

*Launcher Icon*
![Launcher Icon](img/launcher.png)


### Assignment Links:

[Assignment 4 Repo Link](https://bitbucket.org/forbesbkg/lis4331/src/master/a4/ "A4")
