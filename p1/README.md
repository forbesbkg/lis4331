# LIS4331 - Advanced Mobile Web Applications Development

## Bretania Forbes

### Project 1 Requirements:

*Music player app*

1. Include splash screen image, app title, intro text
2. Include artists' images and media
3. Images and buttons must be vertically and horizontally aligned
4. Must add background color(s) or theme
5. Create and display launched icon image


### README.md includes the following items:
* Screenshots of running Music Player App
* Repo and assignment links


### Assignment Screenshots:

*Screenshots of running Music Player App* 

|![Splash Screen](img/splash.png) | ![Play](img/1.png)


|![Pause](img/2.png) | 



### Assignment Links:

[Project 1 Repo Link](https://bitbucket.org/forbesbkg/lis4331/src/master/p1/ "P1")
