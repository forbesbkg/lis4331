package com.example.breespc.newsreader;

import android.annotation.SuppressLint;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

//class stores data for each item in RSS feed: item's title, description, link, and pub date (formatted)

@SuppressLint("SimpleDateFormat")
public class RSSItem{

    private String title = null;
    private String description = null;
    private String link = null;
    private String pubDate = null;

    private SimpleDateFormat dateOutFormat = new SimpleDateFormat("EEEE (MMM d)");  //only includes date, not time
    private SimpleDateFormat dateInFormat = new SimpleDateFormat("yyyy-MM-dd");     //only includes date, not time

    public void setTitle(String title){
        this.title = title;
    }

    public String getTitle(){
        return title;
    }

    public void setDescription(String description){
        this.description = description;
    }

    public String getDescription(){
        return description;
    }

    public void setLink(String link){
        this.link = link;
    }

    public String getLink(){
        return link;
    }

    public void setPubDate(String pubDate){
        this.pubDate = pubDate;
    }

    public String getPubDate(){
        return pubDate;
    }

    public String getPubDateFormatted(){
        try{
            if(pubDate != null){        //make sure pubDate exists
                Date date = dateInFormat.parse(pubDate);
                String pubDateFormatted = dateOutFormat.format(date);
                return pubDateFormatted;
            }
            else{
                return "No date in RSS feed";
            }
        }
        catch(ParseException e){
            return "No date in RSS feed";       //don't throw exception
        }
    }

}