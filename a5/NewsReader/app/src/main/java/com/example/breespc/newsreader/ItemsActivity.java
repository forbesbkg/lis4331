package com.example.breespc.newsreader;

import android.support.v7.app.AppCompatActivity;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.SimpleAdapter;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.HashMap;

public class ItemsActivity extends AppCompatActivity {

    private RSSFeed feed;
    private FileIO io;

    private TextView titleTextView;
    private ListView itemsListView;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_items);

        io = new FileIO(getApplicationContext());

        titleTextView = (TextView) findViewById(R.id.titleTextView);
        itemsListView = (ListView) findViewById(R.id.itemsListView);

        itemsListView.setOnClickListener(this);

        new DownloadFeed().execute();

        //DownloadFeed (inner class) uses background thread to download XML for RSS feed
        class DownloadFeed extends AsyncTask<Void, Void, Void>{
            @Override
            protected Void doInBackground(Void...params){
                io.downloadFile();
                return null;
            }

            //displays message in LogCat view
            @Override
            protected void onPostExecute(Void result){
                Log.d("News reader", "Feed downloaded");
                new ReadFeed().execute();
            }
        }

        //ReadFeed: background thread to read XML for RSS feed and parse into RSSFeed object
        class ReadFeed extends AsyncTask<Void, Void, Void>{
            @Override
            protected Void doInBackground(Void...params){
                feed = io.readFile();
                return null;
            }

            //displays message in LogCat view
            @Override
            protected void onPostExecute(Void result){
                Log.d("News reader", "Feed read");

                //update the display for the activity
                ItemsActivity.this.updateDisplay();
            }
        }

        //updates UI
        public void updateDisplay() {
            if (feed == null) {
                titleTextView.setText("Unable to get RSS feed");
                return;
            }

            //set title for feed
            titleTextView.setText(feed.getTitle());

            //get items for feed
            ArrayList<RSSItem> items = feed.getAllItems();

            //create list of Map objects
            ArrayList<HashMap<String, String>> data = new ArrayList<>();
            for (RSSItem item : items) {
                HashMap<String, String> map = new HashMap<>();
                map.put("date", item.getPubDateFormatted());
                map.put("title", item.getTitle());
                data.add(map);
            }

        }

            int resource = R.layout.listview_item;
            String[] from = {"date", "title"};
            int[] to = {R.id.pubDateTextView, R.id.titleTextView};

            //create and set adapter
            SimpleAdapter adapter = new SimpleAdapter(this, data, resource, from, to);
            itemsListView.setAdapter(adapter);

            Log.d("News Reader", "Feed displayed");

    }

    @Override
    public void onItemClick(AdapterView<?>parent, View v, int position, long id){
        //get the item at the specified position
        RSSItem item = feed.getItem(position);

        //create an intent
        Intent intent = new Intent(this, ItemsActivity.class);

        intent.putExtra("pubdate", item.getPubDate());
        intent.putExtra("title", item.getTitle());
        intent.putExtra("description", item.getDescription());
        intent.putExtra("link", item.getLink());

        this.startActivity(intent);
    }
}
