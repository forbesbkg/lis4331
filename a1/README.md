# LIS4331 - Advanced Mobile Web Applications Development

## Bretania Forbes

### Assignment 1 Requirements:

*Four Parts:*

1. Distributed Version Control with Git and Bitbucket
2. Development Installations
3. Chapter Questions (Ch 1, 2)
4. Bitbucket repo links: a) this assignment and b) the completed tutorials

### README.md includes the following items:
* Screenshot of running java Hello
* Screenshot of running Android Studio - My First App
* Screenshots of running Android Studio - Contacts App
* Git commands with short descriptions

### Git commands w/short descriptions:

1. git init - Creates a new local repository
2. git status - List the files you've changed and those you still need to add or commit 
3. git add - Add one or more files to staging (index)
4. git commit - Commit any files you've added with 'git add', and also commit any files you've 
changed since then
5. git push - Push all branches to your remote repository
6. git pull - Fetch and merge changes on the remote server to your working directory
7. git branch - List all the branches in your repo, also tell you what branch you're currently 
in

### Assignment Screenshots:

*Screenshot of running Java Hello:* |  *Screenshot of running Aandroid Studio - My First App* 

|![Hello World Screenshot](img/hello-world.PNG) | ![My First App](img/MyFirstApp.PNG)

|*Screenshot of Contacts App - Main Screen* |*Screenshot of Contacts App - Info Screen*

|![Contacts Home](img/contact-home.PNG) | ![My First App](img/contact-page.PNG)

### Assignment Links:

[Assignment 1 Repo Link](https://bitbucket.org/forbesbkg/lis4331/src/master/a1/ "A1")