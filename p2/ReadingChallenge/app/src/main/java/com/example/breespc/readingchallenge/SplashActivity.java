package com.example.breespc.readingchallenge;

import android.content.Intent;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import java.util.Timer;
import java.util.TimerTask;

public class SplashActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);

        ActionBar actionBar = getSupportActionBar();
        actionBar.setBackgroundDrawable(getResources().getDrawable(R.drawable.gradient));

        TimerTask task = new TimerTask(){

            @Override
            public void run() {
                finish();
                startActivity(new Intent(SplashActivity.this, ListActivity.class));
            }
        };

        Timer opening = new Timer();
        opening.schedule(task, 3000);
    }
}
