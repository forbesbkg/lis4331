package com.example.breespc.readingchallenge;

public class Item{

    private int itemId;
    private int listId;
    private String name;
    private String read;
    private String hidden;
    private String notes;

    public static final String TRUE = "1";
    public static final String FALSE = "0";

    public Item(){
        name = "";
        read = FALSE;
        hidden = FALSE;
        notes = "";
    }

    public Item(int listId, String name, String read, String hidden, String notes){
        this.listId = listId;
        this.name = name;
        this.read = read;
        this.hidden = hidden;
        this.notes = notes;
    }

    public Item(int itemId, int listId, String name, String read, String hidden, String notes){
        this.itemId = itemId;
        this.listId = listId;
        this.name = name;
        this.read = read;
        this.hidden = hidden;
        this.notes = notes;
    }

    public int getItemId(){
        return itemId;
    }

    public void setItemId(int itemId){
        this.itemId = itemId;
    }

    public int getListId(){
        return listId;
    }

    public void setListId(int listId){
        this.listId = listId;
    }

    public String getName(){
        return name;
    }

    public void setName(String name){
        this.name = name;
    }

    public String getRead(){
        return read;
    }

    public void setRead(String read){
        this.read = read;
    }

    public String getHidden(){
        return hidden;
    }

    public void setHidden(String hidden){
        this.hidden = hidden;
    }

    public String getNotes(){
        return notes;
    }

    public void setNotes(String notes){
        this.notes = notes;
    }
}