package com.example.breespc.readingchallenge;

import java.util.ArrayList;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteDatabase.CursorFactory;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

public class ReadingChallengeDB{
    //database constants
    public static final String DB_NAME = "readingchallenge.db";
    public static final int DB_VERSION = 1;

    //challenge table constants
    public static final String CHALLENGE_TABLE = "challenge";
    
    public static final String CHALLENGE_ID = "chl_id";
    public static final int CHALLENGE_ID_COL = 0;

    public static final String CHALLENGE_NAME = "chl_name";
    public static final int CHALLENGE_NAME_COL = 1;

    //challenge item table constants
    public static final String ITEM_TABLE = "item";
    
    public static final String ITEM_ID = "itm_id";
    public static final int ITEM_ID_COL = 0;

    public static final String CHALLENGE_LIST_ID = "chl_id";
    public static final int CHALLENGE_LIST_ID_COL = 1;

    public static final String ITEM_NAME = "itm_name";
    public static final int ITEM_NAME_COL = 2;

    public static final String ITEM_DATE_READ = "itm_read";
    public static final int ITEM_DATE_READ_COL = 3;

    public static final String ITEM_HIDDEN = "itm_hidden";
    public static final int ITEM_HIDDEN_COL = 4;

    public static final String ITEM_NOTES = "itm_notes";
    public static final int ITEM_NOTES_COL = 5;

    //CREATE and DROP TABLE statements
    public static final String CREATE_CHALLENGE_TABLE = 
        "CREATE TABLE " + CHALLENGE_TABLE + " (" +
        CHALLENGE_ID + " INTEGER PRIMARY KEY AUTOINCREMENT, " +
        CHALLENGE_NAME + " TEXT NOT NULL UNIQUE);";

    public static final String CREATE_ITEM_TABLE = 
        "CREATE TABLE " + ITEM_TABLE + " (" +
        ITEM_ID             + "INTEGER PRIMARY KEY AUTOINCREMENT, " +
        CHALLENGE_LIST_ID   + " INTEGER NOT NULL, " +
        ITEM_NAME           + " TEXT NOT NULL, " +
        ITEM_DATE_READ           + "TEXT, " +
        ITEM_HIDDEN         + "TEXT, " +
        ITEM_NOTES          + "TEXT);";

    public static final String DROP_CHALLENGE_TABLE =
        "DROP TABLE IF EXISTS " + CHALLENGE_TABLE;

    public static final String DROP_ITEM_TABLE = 
        "DROP TABLE IF EXISTS " + ITEM_TABLE;

    private static class DBHelper extends SQLiteOpenHelper{

        public DBHelper(Context context, String name, CursorFactory factory, int version){
            super(context, name, factory, version);
        }

        @Override
        public void onCreate(SQLiteDatabase db){
            db.execSQL(CREATE_CHALLENGE_TABLE);
            db.execSQL(CREATE_ITEM_TABLE);

            //insert default challenge
            db.execSQL("INSERT INTO " + CHALLENGE_TABLE + " VALUES (1, 'Pop Sugar Reading Challenge')");
            db.execSQL("INSERT INTO " + CHALLENGE_TABLE + " VALUES (2, 'Non Fiction')");

            //insert sample challenge items
            db.execSQL("INSERT INTO " + ITEM_TABLE + " VALUES (1, 1, 'Book set in space', '0', '0', 'Nyxia by Scott Reintgen')");
            db.execSQL("INSERT INTO " + ITEM_TABLE + " VALUES (2, 2, 'Book about money', '0', '0', 'Set For Life by Scott Trench')");
        }

        @Override
        public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion){
            Log.d("Challenge list", "Upgrading db from version " + oldVersion + " to " + newVersion);
            
            db.execSQL(ReadingChallengeDB.DROP_CHALLENGE_TABLE);
            db.execSQL(ReadingChallengeDB.DROP_ITEM_TABLE);
            onCreate(db);
        }   

    }

    //database object and database helper object
    private SQLiteDatabase db;
    private DBHelper dbHelper;

    public ReadingChallengeDB(Context context){
        dbHelper = new DBHelper(context, DB_NAME, null, DB_VERSION);
    }

    private void openReadableDB(){
        db = dbHelper.getReadableDatabase();
    }

    private void openWriteableDB(){
        db = dbHelper.getWritableDatabase();
    }

    private void closeDB(){
        if (db != null){
            db.close();
        }
    }

    public List getList(String name){
        String where = CHALLENGE_NAME + "= ?";
        String[] whereArgs = { name };

        openReadableDB();
        Cursor cursor = db.query(CHALLENGE_NAME, null, where, whereArgs, null, null, null);
        List list = null;

        cursor.moveToFirst();

        list = new List(cursor.getInt(CHALLENGE_ID_COL), cursor.getString(CHALLENGE_NAME_COL));
        if (cursor != null)
            cursor.close();
        this.closeDB();

        return list;
    }

    public ArrayList<Item> getItems(String listName){
        String where = 
                CHALLENGE_ID + "= ? AND " +
                ITEM_HIDDEN + "!= '1'";
        int listID = getList(listName).getId();
        String[] whereArgs = { Integer.toString(listID) };

        this.openReadableDB();
        Cursor cursor = db.query(ITEM_TABLE, null, where, whereArgs, null, null, null);
        ArrayList<Item> items = new ArrayList<>();
        while (cursor.moveToNext()){
            items.add(getItemFromCursor(cursor));
        }
        if (cursor != null){
            cursor.close();
        }
        this.closeDB();

        return items;
    }

    public Item getItem(int id){
        String where = ITEM_ID + "= ?";
        String[] whereArgs = {Integer.toString(id)};

        this.openReadableDB();
        Cursor cursor = db.query(ITEM_TABLE, null, where, whereArgs, null, null, null);
        cursor.moveToFirst();
        Item item = getItemFromCursor(cursor);
        if (cursor != null){
            cursor.close();
        }
        this.closeDB();

        return item;
    }

    private static Item getItemFromCursor(Cursor cursor){
        if (cursor == null || cursor.getCount() == 0){
            return null;
        }
        else{
            try{
                Item item = new Item(
                    cursor.getInt(ITEM_ID_COL),
                    cursor.getInt(CHALLENGE_LIST_ID_COL),
                    cursor.getString(ITEM_NAME_COL),
                    cursor.getString(ITEM_DATE_READ_COL),
                    cursor.getString(ITEM_HIDDEN_COL),
                    cursor.getString(ITEM_NOTES_COL));
                return item;
            }
            catch(Exception e){
                return null;
            }
        }
    }

    public long insertItem(Item item){
        ContentValues cv = new ContentValues();
        cv.put(CHALLENGE_LIST_ID, item.getListId());
        cv.put(ITEM_NAME, item.getName());
        cv.put(ITEM_DATE_READ, item.getRead());
        cv.put(ITEM_HIDDEN, item.getHidden());
        cv.put(ITEM_NOTES, item.getNotes());

        this.openWriteableDB();
        long rowID = db.insert(ITEM_TABLE, null, cv);
        this.closeDB();

        return rowID;
    }

    public int updateItem(Item item){
        ContentValues cv = new ContentValues();
        cv.put(CHALLENGE_LIST_ID, item.getListId());
        cv.put(ITEM_NAME, item.getName());
        cv.put(ITEM_DATE_READ, item.getRead());
        cv.put(ITEM_HIDDEN, item.getHidden());
        cv.put(ITEM_NOTES, item.getNotes());

        String where = ITEM_ID + "= ?";
        String[] whereArgs = { String.valueOf(item.getItemId()) };

        this.openWriteableDB();
        int rowCount = db.update(ITEM_TABLE, cv, where, whereArgs);
        this.closeDB();

        return rowCount;
    }

    public int deleteItem(long id){
        String where = ITEM_ID + "= ?";
        String[] whereArgs = { String.valueOf(id) };

        this.openWriteableDB();
        int rowCount = db.delete(ITEM_TABLE, where, whereArgs);
        this.closeDB();

        return rowCount;
    }

}