# LIS4331 - Advanced Mobile Web Applications Development

## Bretania Forbes

### Project 2 Requirements:

*Task list app*

1. Include splash screen image, app title, intro text
2. Insert at least five sample tasks
3. Test database class
4. Must add background color(s) or theme
5. Create and display launcher icon image


### README.md includes the following items:
* Screenshots of running Task List App
* Repo and assignment links


### Assignment Screenshots:

*Screenshots of running Task List App* 

|![Splash Screen](img/1.png) | ![List](img/2.png)




### Assignment Links:

[Project 2 Repo Link](https://bitbucket.org/forbesbkg/lis4331/src/master/p2/ "P2")
