# LIS4331 - Advanced Mobile Web Applications Development

## Bretania Forbes

### Assignment 2 Requirements:

*Creation of app that allows user to split bills between 1-10 people.*

1. Drop-down menu for total number of guests (including yourself): 1-10
2. Drop-down menu for tip percentage (5% increments): 0-25
3. Must add background color(s) or theme
4. Must create and display launcher icon image

### README.md includes the following items:
* Screenshots of running Tip Calculator App
* Repo and assignment links


### Assignment Screenshots:

*Screenshots of running Tip Calculator App* 

|![Unpopulated Tip Calculator](img/tip1.png) | ![tip amount screenshot](img/tip2.png)


|![Tip Populated](img/tip3.png)

|![Unpopulated Split Bill Calculator](img/split1.png) | ![guest amount screenshot](img/split2.png)

|![tip amount screenshot](img/split3.png) | ![split populated screenshot](img/split4.png)

*Launcher Icon*
![Launcher Icon](img/launcher.png)


### Assignment Links:

[Assignment 2 Repo Link](https://bitbucket.org/forbesbkg/lis4331/src/master/a2/ "A2")
