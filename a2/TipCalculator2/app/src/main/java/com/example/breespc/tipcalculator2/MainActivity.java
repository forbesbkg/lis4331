package com.example.breespc.tipcalculator2;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;

import java.text.DecimalFormat;

public class MainActivity extends AppCompatActivity {

    double billAmt = 0, tipAmt = 0, totalCost = 0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        TextView splitBill = (TextView) findViewById(R.id.txtLabel2a);
        Button btnCalcTip = (Button) findViewById(R.id.btnCaclTip);
        final EditText billAmount = (EditText)findViewById(R.id.inputBill1);
        final Spinner spinnerTip1 = (Spinner)findViewById(R.id.spinnerTip1);


       splitBill.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v){
                startActivity(new Intent (MainActivity.this, SplitBill.class));
            }
        });

       btnCalcTip.setOnClickListener(new View.OnClickListener(){
           final TextView result = (TextView)findViewById(R.id.txtResult1);
           String percent = "";

           @Override
           public void onClick(View v){
               percent = spinnerTip1.getSelectedItem().toString();
               percent = percent.substring(0, (percent.length() -1));

               billAmt = Double.parseDouble(billAmount.getText().toString());
               tipAmt = (Double.parseDouble(percent)) / 100;

               totalCost = (billAmt * tipAmt) + billAmt;
               DecimalFormat currency = new DecimalFormat("$###,###.##");

               result.setText("Tip amount for $" + billAmt + " is " + currency.format(tipAmt * billAmt) +
                       ". Total bill is " + currency.format(totalCost));

           }
       });
    }
}
