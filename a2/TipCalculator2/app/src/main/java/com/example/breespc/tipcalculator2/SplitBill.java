package com.example.breespc.tipcalculator2;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;
import java.text.DecimalFormat;

public class SplitBill extends AppCompatActivity {

    int guestAmt = 0;
    double billAmt = 0, tipAmt = 0, totalCost = 0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_split_bill);

        TextView tipCalc = (TextView)findViewById(R.id.txtLabel1b);
        Button btnCalcTip = (Button)findViewById(R.id.btnCalcTotal);
        final EditText billAmount = (EditText)findViewById(R.id.inputBill2);
        final Spinner spinnerGuests = (Spinner)findViewById(R.id.spinnerGuests);
        final Spinner spinnerTip2 = (Spinner)findViewById(R.id.spinnerTip2);


        tipCalc.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v){
                startActivity(new Intent(SplitBill.this, MainActivity.class));
            }
        });

        btnCalcTip.setOnClickListener(new View.OnClickListener(){
            final TextView result1 = (TextView)findViewById(R.id.txtResult2);
            String percent = "";

            @Override
            public void onClick(View v){
                percent = spinnerTip2.getSelectedItem().toString();
                percent = percent.substring(0, percent.length() -1);

                billAmt = Double.parseDouble(billAmount.getText().toString());
                guestAmt = Integer.parseInt(spinnerGuests.getSelectedItem().toString());
                tipAmt = (Double.parseDouble(percent)) / 100;

                totalCost = (billAmt * tipAmt) + billAmt;
                DecimalFormat currency = new DecimalFormat("$###,###.##");

                result1.setText("Cost for each of " + guestAmt + " guests is " + currency.format(totalCost/guestAmt));

            }
        });




    }
}
