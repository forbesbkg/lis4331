package com.example.breespc.mymusic;

import android.media.MediaPlayer;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

public class MainActivity extends AppCompatActivity {

    Button btn1, btn2, btn3, btn4;
    MediaPlayer mpJordan, mpKojey, mpJean, mpNaji;
    int playing;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);



        btn1 = (Button) findViewById(R.id.btnAlbum1);
        btn2 = (Button) findViewById(R.id.btnAlbum2);
        btn3 = (Button) findViewById(R.id.btnAlbum3);
        btn4 = (Button) findViewById(R.id.btnAlbum4);

        mpJordan = new MediaPlayer();
        mpJordan = MediaPlayer.create(this, R.raw.carnation);
        mpKojey = new MediaPlayer();
        mpKojey = MediaPlayer.create(this, R.raw.if_only);
        mpJean = new MediaPlayer();
        mpJean = MediaPlayer.create(this, R.raw.sans_titre);
        mpNaji = new MediaPlayer();
        mpNaji = MediaPlayer.create(this, R.raw.misfit);

        playing = 0;

        btn1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                switch(playing){
                    case 0:
                        mpJordan.start();
                        playing = 1;
                        btn1.setText("Pause Jordan Rakei");
                        btn2.setVisibility(View.INVISIBLE);
                        btn3.setVisibility(View.INVISIBLE);
                        btn4.setVisibility(View.INVISIBLE);

                        break;
                    case 1:
                        mpJordan.pause();
                        playing = 0;
                        btn1.setText("Jordan Rakei");
                        btn2.setVisibility(View.VISIBLE);
                        btn3.setVisibility(View.VISIBLE);
                        btn4.setVisibility(View.VISIBLE);
                        break;
                }

            }

        });

        btn2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                switch(playing){
                    case 0:
                        mpKojey.start();
                        playing = 1;
                        btn2.setText("Pause Kojey Radical");
                        btn1.setVisibility(View.INVISIBLE);
                        btn3.setVisibility(View.INVISIBLE);
                        btn4.setVisibility(View.INVISIBLE);

                        break;
                    case 1:
                        mpKojey.pause();
                        playing = 0;
                        btn2.setText("Kojey Radical");
                        btn1.setVisibility(View.VISIBLE);
                        btn3.setVisibility(View.VISIBLE);
                        btn4.setVisibility(View.VISIBLE);
                        break;
                }

            }

        });

        btn3.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                switch(playing){
                    case 0:
                        mpJean.start();
                        playing = 1;
                        btn3.setText("Pause Jean-Michel Blais");
                        btn2.setVisibility(View.INVISIBLE);
                        btn1.setVisibility(View.INVISIBLE);
                        btn4.setVisibility(View.INVISIBLE);

                        break;
                    case 1:
                        mpJean.pause();
                        playing = 0;
                        btn3.setText("Jean-Michel Blais");
                        btn2.setVisibility(View.VISIBLE);
                        btn1.setVisibility(View.VISIBLE);
                        btn4.setVisibility(View.VISIBLE);
                        break;
                }

            }

        });

        btn4.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                switch(playing){
                    case 0:
                        mpNaji.start();
                        playing = 1;
                        btn4.setText("Pause Naji");
                        btn2.setVisibility(View.INVISIBLE);
                        btn3.setVisibility(View.INVISIBLE);
                        btn1.setVisibility(View.INVISIBLE);

                        break;
                    case 1:
                        mpNaji.pause();
                        playing = 0;
                        btn4.setText("Naji");
                        btn2.setVisibility(View.VISIBLE);
                        btn3.setVisibility(View.VISIBLE);
                        btn1.setVisibility(View.VISIBLE);
                        break;
                }

            }

        });
    }
}
