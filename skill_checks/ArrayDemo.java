public class ArrayDemo{

    private int [] arr;
    public static void main (String [] args){
        ArrayDemo array = new ArrayDemo();
        array.reverseArray();
        array.sumArray();
        System.out.println("\nSum of all numbers is " + array.sumArray());
        System.out.println("Average is " + array.avgArray());
        array.greaterArray();

    }

    public ArrayDemo(){
        arr = new int []{12, 15, 34, 67, 4, 9, 10, 7, 13, 50};
        System.out.print("\nNumbers in the array are ");
        for(int num: arr){
            System.out.print(num + " ");
        }
    }

    public void reverseArray(){
        int reverse[] = new int[arr.length];
        int j = arr.length - 1;
        for (int i = 0; i < arr.length; i++){
            reverse[j] = arr[i];
            j-=1;
        }
        
        System.out.print("\nNumbers in reverse order are ");

        for (int num: reverse){
            System.out.print(num + " ");
        }
    }

    public int sumArray(){
        int sum = 0;
        for (int num: arr){
            sum += num;
        }
        return sum;
    }

    public double avgArray(){
        double avg = this.sumArray() * 1.0 / arr.length;
        return avg;
    }

    public void  greaterArray(){
        for (int num: arr){
            if (num > this.avgArray()){
                System.out.print(num + " ");
            }
        }

        System.out.println("are greater than the average\n");
    }
}