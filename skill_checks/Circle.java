import java.util.Scanner;

public class Circle{

    public static void main(String []args){

        double radius = 0.0, PI = 3.1415;
        Scanner read = new Scanner(System.in);

        System.out.print("Enter circle radius: ");

        while (!read.hasNextDouble()){
            System.out.println("Not valid number!\n");
            read.next();
            System.out.print("Please try again. Enter circle radius: ");
        }

        radius = read.nextDouble();

        System.out.printf("\nCircle diameter: %.2f\n",  (radius * 2));
        System.out.printf("Circumfrence: %.2f\n",  (radius * 2 * PI));
        System.out.printf("Area: %.2f\n", (radius * radius * PI));

        read.close();


        
        
       




    }
}