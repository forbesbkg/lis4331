import java.util.Scanner;
import java.util.ArrayList;
import java.io.PrintWriter;
import java.io.IOException;
import java.io.BufferedReader;
import java.io.FileReader;
import java.io.FileNotFoundException;

public class FileReadWrite{

    public static void main(String [] args) throws IOException{

        Scanner read = new Scanner(System.in);
        String file = "filewrite.txt";
        String line, text;
        PrintWriter out = new PrintWriter(file);
        BufferedReader fileRead;

        System.out.print("Please enter text: ");
        text = read.nextLine();
        out.println(text);

        read.close();
        out.close();



        System.out.println("\nPrinting text from "  + file + "\n");
        
        fileRead = new BufferedReader(new FileReader(file));
        
        while((line = fileRead.readLine()) != null){
            System.out.println(line + "\n");
        }

        fileRead.close();

    }
}