import java.util.Scanner;
import java.text.NumberFormat;

public class SimpleInterestCalculator{

    public static void main (String [] args){

        Scanner read = new Scanner(System.in);
        double principal, interest, totalAmount;
        int years;

        System.out.print("\nCurrent principal: $");

        while(!read.hasNextDouble()){
            System.out.println("Not valid number!\n");
            read.next();
            System.out.print("Please try again. Enter principal: $");
        }

        principal = read.nextDouble();

        System.out.print("\nInterest Rate (per year): ");

        while(!read.hasNextDouble()){
            System.out.println("Not valid number!\n");
            read.next();
            System.out.print("Please try again. Enter interest rate: ");
        }
       
        interest = read.nextDouble();

        System.out.print("\nTotal time (in years): ");

        while(!read.hasNextInt()){
            System.out.println("Not valid integer!\n");
            read.next();
            System.out.print("Please try again. Enter years: ");
        }

        years = read.nextInt();

        totalAmount = ((principal * (interest/100)) * years) + principal;

        NumberFormat nf = NumberFormat.getCurrencyInstance();

        System.out.println("You will have saved " + nf.format(totalAmount) + " in " + years + " years, at an interest rate of " + interest + "%\n\n");

        read.close();

    }
}