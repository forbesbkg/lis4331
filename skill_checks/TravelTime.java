import java.util.Scanner;

public class TravelTime{

    public static void main(String [] args){

        int hours, minutes;
        double miles = 0, mph = 0, mpm = 0, time =0;
        String choice = "y";
        Scanner read = new Scanner(System.in);

        do{
            System.out.print("\nPlease enter miles: ");

            while (!read.hasNextDouble() ){
                System.out.println("Invalid double-- miles must be a number. \n");
                read.next();
                System.out.print("Please enter miles: ");
            }

            miles = read.nextDouble();

            while(miles < 0 || miles > 3000){
                System.out.println("Miles must be greater than 0, and no more than 3000.\n");
                //read.next();
                System.out.print("Please enter miles: ");
                miles = read.nextDouble();
            }

            System.out.print("Please enter MPH: ");

            while(!read.hasNextDouble()){
               
                System.out.println("Invalid double--MPH must be a number.\n");

                read.next();
                System.out.print("Please enter MPH: ");

            }
            mph = read.nextDouble();

            while(mph < 0 || mph > 100){
                System.out.println("MPH must be greater than 0, and no more than 100.\n");
                //read.next();
                System.out.print("Please enter MPH: ");
                mph = read.nextDouble();
            }

            

            mpm = mph / 60;
            time = miles/ mpm;

            hours = (int) time / 60;
            minutes = (int) time % 60;


            System.out.printf("Estimated travel time: %d hr(s) %d Minutes\n", hours, minutes);

           /* do{            
                System.out.print("Continue? (y/n): ");
                choice = read.next();
            }while(!choice.equals("y") || !choice.equals("n"));*/

            System.out.print("Continue? (y/n): ");
            choice = read.next();

        }while (choice.equals("y"));

        read.close();
    }

}