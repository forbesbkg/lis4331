import java.util.Scanner;

public class BookDemo{

    public static void main(String [] args){
        
        Product p1 = new Product();
        Product p2 = new Product("def456", "Your Widget", 59.99);
        Scanner read = new Scanner(System.in);
        String code, desc, author;
        double price;

        System.out.print("\n////Below using setter methods to pass literal values to p2, then print() method to display values://///\n");
        
        p2.setCode("xyz789");
        p2.setDescription("Test Widget");
        p2.setPrice(89.99);

        p2.print();

        System.out.println("////Below are *derived class default constructor* values (instantiating b1, then using getter methods)://///\n");
        Book b1 = new Book();

        System.out.println("Or using overridden derived class print() method...\n");
        b1.print();

        System.out.println("////Below are *derived class* user-entered values (instantiating b2, then using getter methods):////");

        System.out.print("Code: ");
        code = read.nextLine();
        System.out.print("Description: ");
        desc = read.nextLine();
        System.out.print("Price: ");
        price = read.nextDouble();
        read.nextLine();
        System.out.print("Author: ");
        author = read.nextLine();

        Book b2 = new Book(code, desc, price, author);

        System.out.println("\nOr using derived class print() method\n");
        b2.print();

        read.close();



    }
}