
public class Book extends Product{

    private String author;

    public Book(){
        super();
        author = "John Doe";
    }

    public Book(String c, String d, double p, String a){
        super(c, d, p);
        author = a;
    }

    
    public String getAuthor(){
        return author;
    }

    public void setAuthor(String a){
        author = a;
    }


    public void displayInfoD(){
        System.out.println("Inside default book constructor");
        super.displayInfoD();
        System.out.println("Author: "+ author);
    }

    public void displayInfoP(){
        System.out.println("Inside book constructor with parameters");
        super.displayInfoP();
        System.out.println("Author: "+ author);
    }

    public void print(){
        System.out.println("Code: " + super.getCode() +
                            ", Description: " + super.getDescription()+
                            ", Price: $" + super.getPrice() +
                            ", Author: " + author + '\n');
    }


}



