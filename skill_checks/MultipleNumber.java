import java.util.Scanner;

public class MultipleNumber{

    public static void main(String []args){

        int num1, num2;
        Scanner read = new Scanner(System.in);

        System.out.print("Enter Num1: ");

        while (!read.hasNextInt()){
            System.out.println("Not valid integer!\n");
            read.next();
            System.out.print("Please try again. Enter Num1: ");
        }
        num1 = read.nextInt();

        System.out.print("Num2: ");

        while (!read.hasNextInt()){
            System.out.println("Not valid integer!\n");
            read.next();
            System.out.print("Please try again. Enter Num2: ");
        }

        num2 = read.nextInt();

        if (num1 % num2 == 0){
            System.out.println(num1 + " is a multiple of " + num2);
            System.out.println("The product of " + (num1 / num2) + " and " + num2 + " is " + num1 );
        }
        else{
            System.out.println(num1 + " is not a multiple of " + num2);
        }


        read.close();


        


    }
}