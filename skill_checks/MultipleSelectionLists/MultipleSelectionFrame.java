import java.awt.FlowLayout;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import javax.swing.JFrame;
import javax.swing.JList;
import javax.swing.JButton;
import javax.swing.JScrollPane;
import javax.swing.ListSelectionModel;

public class MultipleSelectionFrame extends JFrame{
    private JList<String> sushiJList;
    private JList copyJList;
    private JButton copyJButton;
    private static final String[] sushiNames = 
    {"Spicy Tuna", "Shrimp Tempura", "California", "Boston", "King Crab", "Spicy Crab", "Philadelphia", "Alaska", "Avocado", "Dragon" };

    public MultipleSelectionFrame()
    {
        super ("Multiple Selection Lists");
        setLayout (new FlowLayout()); 
        sushiJList = new JList<>(sushiNames);
        sushiJList.setVisibleRowCount(5);
        sushiJList.setFixedCellWidth(140);
        sushiJList.setFixedCellHeight(15);
        sushiJList.setSelectionMode(ListSelectionModel.MULTIPLE_INTERVAL_SELECTION);

        add(new JScrollPane(sushiJList));
        copyJButton = new JButton("Copy >>>");
        copyJButton.addActionListener(
            new ActionListener()
            {
                public void actionPerformed(ActionEvent event)
                {
                    copyJList.setListData(sushiJList.getSelectedValues());
                }
            }
        );
        
        add(copyJButton);
        copyJList = new JList();

        copyJList.setVisibleRowCount(5);
        copyJList.setFixedCellWidth(140);
        copyJList.setFixedCellHeight(15);
        copyJList.setSelectionMode(ListSelectionModel.SINGLE_INTERVAL_SELECTION);
        add(new JScrollPane(copyJList));

        
        
    }
}