public class Product{

    private String code, description;
    private double price;

    public Product(){
        code = "abc123";
        description="My Widget";
        price = 49.99;

        System.out.println("\nInside product default constructor.\n");
        displayInfo();
        
    }

    public Product(String cd, String desc, double pr){
        code = cd;
        description = desc;
        price = pr;

        System.out.println("\nInside product constructor with parameters.\n");
        displayInfo();

    }

    public void setCode(String cd){
        code = cd;
    }

    public String getCode(){
        return code;
    }

    public void setDescription(String desc){
        description = desc;
    }

    public String getDescription(){
        return description;
    }

    public void setPrice(double pr){
        price = pr;
    }

    public double getPrice(){
        return price;
    }

    public void displayInfo(){

        System.out.println("Code = " + code);
        System.out.println("Description = " + description);
        System.out.println("Price = $" + price);
    }
}