
public class ProductDemo{

    public static void main(String [] args){

        Product prod1 = new Product();
        Product prod2 = new Product("def456", "Your Widget", 59.99);

        prod1.setCode("xyz789");
        prod1.setDescription("Test Widget");
        prod1.setPrice(89.99);

        System.out.println("\n////Below using setter methods to pass literal values, then print() method to display values:////\n");
        System.out.println("Code: " + prod1.getCode() +
                            ", Description: " + prod1.getDescription()+
                            ", Price: $" + prod1.getPrice() + '\n');

    }
}