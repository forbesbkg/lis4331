import java.util.Scanner;

public class MeasurementConversion{

    public static void main(String []args){

        final double INCHES_TO_CENTIMETER = 2.54;
        final double INCHES_TO_METER = 0.0254;
        final double INCHES_TO_FOOT = 12;
        final double INCHES_TO_YARD = 36;
        final double FEET_TO_MILE = 63360;

        int inches;

        Scanner read = new Scanner(System.in);

        // System Requirements
        System.out.println("Program converts inches to centimeters, meters, feet, yards, and miles.");
        System.out.println("***Notes***");
        System.out.println("1) Use integer for inches (must validate user input).");
        System.out.println("2) Use printf() function to print (format values per below output).");
        System.out.println("3) Create Java \"constants\" for the following values: ");
        System.out.println("\tINCHES_TO_CENTIMETER,\n\tINCHES_TO_METER,\n\tINCHES_TO_FOOT,");
        System.out.println("\tINCHES_TO_YARD,\n\tFEET_TO_MILE\n");


        System.out.print("Please enter number of inches: ");
        while (!read.hasNextInt()){
            System.out.println("Not valid integer!\n");
            read.next();
            System.out.print("Please enter number of inches: ");
        }

        inches = read.nextInt();

        System.out.printf("%n%,d inch(es) equals%n", inches);
        System.out.printf("%,.6f centimeter(s%n", (inches*INCHES_TO_CENTIMETER));
        System.out.printf("%.6f meter(s)%n", (inches*INCHES_TO_METER));
        System.out.printf("%.6f feet%n", (inches/INCHES_TO_FOOT));
        System.out.printf("%.6f yard(s)%n", (inches/INCHES_TO_YARD));
        System.out.printf("%.8f miles(s)%n%n", (inches/FEET_TO_MILE));

        read.close();


        
        
    }


}