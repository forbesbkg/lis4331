import java.awt.*;
import java.awt.event.*;
import javax.swing.*;
import java.text.DecimalFormat;

public class DistanceCalculatorGui implements ActionListener{

    public static void main(String [] args){
        DistanceCalculatorGui gui = new DistanceCalculatorGui();
    
    }

    //onscreen components
    private JFrame frame;
    private JTextField legAField;
    private JTextField legBField;
    private JLabel distanceLabel;
    private JButton computeButton;

    public DistanceCalculatorGui(){
        
        legAField = new JTextField(5);
        legBField = new JTextField(5);
        distanceLabel = new JLabel("Compute Distance Leg C");
        computeButton = new JButton("Compute");

        computeButton.addActionListener(this);

        // layout
        JPanel north = new JPanel(new GridLayout(2, 2));
        north.add(new JLabel("Leg A: "));
        north.add(legAField);
        north.add(new JLabel("Leg B: "));
        north.add(legBField);

        // overall frame
        frame = new JFrame("Distance Calculator");
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.setLayout(new BorderLayout());
        frame.add(north, BorderLayout.NORTH);
        frame.add(distanceLabel, BorderLayout.CENTER);
        frame.add(computeButton, BorderLayout.SOUTH);
        frame.pack();
        frame.setVisible(true);

    }

    public void actionPerformed(ActionEvent event){
        
        double legA = Double.parseDouble(legAField.getText().toString());
        double legB = Double.parseDouble(legBField.getText().toString());

        double legC = Math.sqrt((legA * legA) + (legB * legB));

        distanceLabel.setText("Leg C: " + new DecimalFormat("##.00").format(legC));
    }
}