# LIS4331 - Advanced Mobile Web Application Development

## Bretania Forbes

### LIS4331 Requirements:

1. [A1 README.md](https://bitbucket.org/forbesbkg/lis4331/src/master/a1/README.md "Assignment 1 Repo")
    * Install JDK
    * Install Android Studio and create My First App and Contacts App
    * Provide screenshots of installations
    * Create Bitbucket tutorials (bitbucketstationlocations)
    * Provide git command descriptions

2. [A2 README.md](https://bitbucket.org/forbesbkg/lis4331/src/master/a2/README.md "Assignment 2 Repo")
    * Build Tip Calculator App allows user to split a bill between 1 to 10 people
    
    *Requirements*
    
    1. Drop-down menu for total number of guests (including yourself): 1-10
    2. Drop-down menu for tip percentage (5% increments): 0-25
    3. Must add background color(s) or theme
    4. Must create and display launcher icon image

3. [A3 README.md](https://bitbucket.org/forbesbkg/lis4331/src/master/a3/README.md "Assignment 3 Repo")
    * Currency app that converts US Dollars to one of three possible conversions (Euros, Pesos, or Canadian Dollars)

    *Requirements*

    1. Field to enter U.S. dollar amount: 1 - 100,000
    2. Must include toast notification if user enters out-of-range values
    3. Radio buttons to convert from U.S. to Euros, Pesos, and Canadian currency (must be vertically and horizontally aligned)
    4. Must include correct sign for euros, pesos, and Canadian dollars
    5. Must add background color(s) or theme
    6. Create and display launcher icon image


4. [P1 README.md](https://bitbucket.org/forbesbkg/lis4331/src/master/p1/README.md "Project 1 Repo")
    * Music player app that allows you to listen to various artists.

    *Requirements*

    1. Include splash screen image, app title, intro text
    2. Include artists' images and media
    3. Images and buttons must be vertically and horizontally aligned
    4. Must add background color(s) or theme
    5. Create and display launched icon image

5. [A4 README.md](https://bitbucket.org/forbesbkg/lis4331/src/master/a4/README.md "Project 1 Repo")
    * Mortgage calculator app that calculates amount paid over certain number of years

    *Requirements*

    1. Include splash screen image, app title, intro text.
    2. Include appropriate images.
    3. Must use persistent data: SharedPreferences.
    4. Widgets and images must be vertically and horizontally aligned.
    5. Must add background color(s) or theme.
    6. Create and display launched icon image

6. [P2 README.md](https://bitbucket.org/forbesbkg/lis4331/src/master/p2/README.md "Project 2 Repo")
    * Task list app with SQLite

    *Requirements*

    1. Include splash screen image, app title, intro text
    2. Insert at least five sample tasks
    3. Test database class
    4. Must add background color(s) or theme
    5. Create and display launcher icon image