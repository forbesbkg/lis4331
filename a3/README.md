# LIS4331 - Advanced Mobile Web Applications Development

## Bretania Forbes

### Assignment 3 Requirements:

*Currency app that converts US Dollars to one of three possible conversions (Euros, Pesos, or Canadian Dollars)*

1. Field to enter U.S. dollar amount: 1 - 100,000
2. Must include toast notification if user enters out-of-range values
3. Radio buttons to convert from U.S. to Euros, Pesos, and Canadian currency (must be vertically and horizontally aligned)
4. Must include correct sign for euros, pesos, and Canadian dollars
5. Must add background color(s) or theme
6. Create and display launcher icon image


### README.md includes the following items:
* Screenshots of running Currency Converter App
* Repo and assignment links


### Assignment Screenshots:

*Screenshots of running Currency Converter App* 

|![Unpopulated currency converter](img/1.png) | ![toast notif-must enter](img/2.png)


|![toast notif-invalid amount](img/7.png) | ![toast notif-select conversion](img/3.png)

|![Euros screen](img/4.png) | ![pesos screen](img/5.png)

|![CAD screen](img/6.png) |

*Launcher Icon*
![Launcher Icon](img/launcher.png)


### Assignment Links:

[Assignment 3 Repo Link](https://bitbucket.org/forbesbkg/lis4331/src/master/a3/ "A3")
