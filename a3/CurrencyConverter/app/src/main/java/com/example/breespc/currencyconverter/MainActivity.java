package com.example.breespc.currencyconverter;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RadioButton;
import android.widget.TextView;
import android.widget.Toast;

import java.text.DecimalFormat;

public class MainActivity extends AppCompatActivity {

    double convertedCurr, enteredCurr = 0;
    double convertEuro = .88, convertPesos = 19.24, convertCad = 1.32;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        getSupportActionBar().setDisplayShowHomeEnabled(true);
        getSupportActionBar().setLogo(R.mipmap.ic_launcher);
        getSupportActionBar().setDisplayUseLogoEnabled(true);

        final EditText currency = (EditText) findViewById(R.id.editAmt);
        final RadioButton usdToEuro = (RadioButton) findViewById(R.id.radUsdToEuro);
        final RadioButton usdToPesos = (RadioButton) findViewById(R.id.radUsdToPesos);
        final RadioButton usdToCad = (RadioButton) findViewById(R.id.radUsdToCad);
        final TextView result = (TextView) findViewById(R.id.txtResult);
        Button convert = (Button) findViewById(R.id.btnConvert);
        final ImageView flag = (ImageView) findViewById(R.id.imgFlag);

        convert.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (currency.getText().toString().isEmpty()){
                    Toast.makeText(MainActivity.this, "Please enter US Dollars. Must be > 0 and <= 100,000", Toast.LENGTH_LONG).show();
                }
                else{
                    enteredCurr = Double.parseDouble(currency.getText().toString());
                    DecimalFormat formatCurr = new DecimalFormat("###,###.##");

                    if (enteredCurr < 1 || enteredCurr > 100000){
                        Toast.makeText(MainActivity.this, "US Dollars must be > 0 and <= 100,000", Toast.LENGTH_LONG).show();
                    }
                else {
                        if (usdToEuro.isChecked()){
                            flag.setImageResource(R.drawable.eu_flag);
                            convertedCurr = enteredCurr * convertEuro;
                            result.setText("€" + formatCurr.format(convertedCurr) + " Euros");
                        }
                        else if (usdToPesos.isChecked()){
                            flag.setImageResource(R.drawable.mexican_flag);
                            convertedCurr = enteredCurr * convertPesos;
                            result.setText("$" + formatCurr.format(convertedCurr) + " Pesos");
                        }
                        else if (usdToCad.isChecked()){
                            flag.setImageResource(R.drawable.canadian_flag);
                            convertedCurr = enteredCurr * convertCad;
                            result.setText("$" + formatCurr.format(convertedCurr) + " Canadian Dollars");
                        }

                        else{
                            Toast.makeText(MainActivity.this, "Must select a conversion", Toast.LENGTH_LONG).show();
                        }
                    }
                }

            }
        });
    }
}
